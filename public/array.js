var arr = [11,22,"haha","computer","bug",33]
/*
从下标1（11）开始，删除了4个元素（22,"haha","computer","bug"）
添加了四个元素("ping","pingzi",88,99)
*/ 
arr.splice(1,4,"ping","pingzi",88,99)
console.log(arr) // [11, "ping", "pingzi", 88, 99, 33]


/*
// 会改变源数组的数据
copyWidthin
shift
unshift
pop
push
fill
*/

// 复制数组一本身
let arr1 = [1, 2, 3, 66, 4, 5]
arr1.copyWithin(3)   //[1, 2, 3, 66， 4, 5]
console.log(arr1) // ) [1, 2, 3, 1, 2，3]

// 数组填充，填充值任何类型都可以
let arr6 = [22, 33, 4, 55, 6, 1, 999]
arr6.fill("pingzi", 5, 6)
console.log(arr6)
// -----------------------------------
// shift删除了数组第一个元素
let arr2 = [22, 33, 4, 55, 6]
arr2.shift()
console.log(arr2)

// pop 删除数组末尾一个
let arr4 = [22, 33, 4, 55, 6, 1]
arr4.pop()
console.log(arr4)
// -----------------------------------
// unshift数组前面添加一个元素
let arr3 = [22, 33, 4, 55, 6]
arr3.unshift(88)
console.log(arr3)

// push 数组末尾添加一个元素
let arr5 = [22, 33, 4, 55, 6, 1, 999]
arr5.push(888)
console.log(arr5)


