const { db } = require('../public/db/db')

//  用户模块数据持久化

const user = {
  // 根据手机号获取用户信息
  getUserByPhone: async (phone) => {
    const sql = 'SELECT id, phone, username FROM login where phone = ?'
    return await db(sql, [phone])
    // db(sql, [phone]).then(res => {

    // })
  },
  // add user
  // {name:"",phone:"",password:""}
  addUser: async (user) => {
    const sql = 'insert into login set ?'
    return await (sql, user)
  },
  // update user
  //  [user, id] => [{nickname:'', age:""}, id]
  update: async (arr) => {
    const sql = 'update login set ? where id=?'
    return await db(sql, arr)
  },
  // delete user
  del: (id) => {
    // 删除通过伪删除
    // 处理this指向问题
    return user.update([{ is_del: 1 }, id])
  },
  // 获取所有用户信息
  getAll: async () => {
    const sql = "select id, phone,password, username where is_del"
    return await db(sql)
  }
}
module.exports = user