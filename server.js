// let http = require("http");
// let fetch = require("node-fetch")

// const fs = require("fs")
// let mysql = require('mysql');
let { db } = require('./public/db/db')
const express = require('express')
const routerapp = require("./public/routers")
const app = express()
const user = require('./manage/user')
// 静态资源路径配置
app.use('/static', express.static(__dirname + '/static'))
// let router = express.Router()
// const path = require('path');
// const { json } = require("express");
// const imgPath = path.join(__dirname, 'public', '6.jpeg')

// const docPath = path.join(__dirname, 'public', '瓶子.doc')


// console.log("图片中径：", imgPath)
// app.use(express.static(path.join(__dirname, 'public')))
// console.log(app.use(express.static(path.join(__dirname, 'public'))))


// let server = http.createServer(function (req, res) {
//   res.write("<head><meta charset='utf-8'/></head>")
//   console.log("服务器收到请求" + req.url)
//   console.info("请求成功")
//   res.write("hello word 龚浩鑫")
//   // 响应断开
//   res.end();
// });
// // http://192.168.10.44:1888/
// server.listen(3000, '192.168.10.44');

// let connection = mysql.createConnection({
//   host: 'localhost',
//   user: 'root',
//   password: '123456',
//   database: 'haoxindb'
// });

// connection.connect();

class Resp {
  constructor(code, msg, data) {
    this.code = code;
    this.msg = msg;
    this.data = data
  }
  static success (data) {
    return new Resp(200, "请求成功", data)
  }
  static error (msg) {
    msg = msg ? msg : "请求失败"
    return new Resp(500, msg, null)
  }
  static nllogin () {
    return new Resp(5010, "用户未登录", null)
  }
}

// home page
app.get("/", async (req, respon) => {
  routerapp.read("pages/index.html").then(res => {
    respon.write(res)
  })
  // const data = await routerapp.read("pages/index.html")
  // res.end(data)
})

// login page
app.get("/login", async (req, res) => {
  const data = await routerapp.read("pages/login.html")
  res.end(data)
})

// insert data
// app.get("/addUser", (req, respon) => {
//   console.log("接收的参数：", req)
//   const sql = "insert into login set ?"
//   const querys = { username: "computer001", password: "777777" }
//   db(sql, querys).then(res => {
//     respon.send(res)
//   })
// })


// app.get("/getUser", (req, respon) => {
//   const sql = "select * from login"
//   db(sql).then(res => {
//     respon.send(res)
//   })
// })


// 业务
// 判断用户是否已存在
app.get("/user/getUserByPhone", async (req, resp) => {
  // 返回数据库数据
  const use = await user.getUserByPhone('152115611221')
  if (use.length > 0) {
    resp.send(Resp.error("用户已存在"))
  } else {
    resp.send(Resp.success())
  }
  // console.log("user:", use)
  // resp.send(use)
})

// 获取验证码
app.get("/getCode", (req, resp) => {
  let code = Math.floor(Math.random() * 10000)
  // req.session.code = code
  console.log("code", code)
})

app.get("/user/addUser", async (req, resp) => {
  let query = { id: 16, username: "haha", password: "12300000", phone: "18000009999" }
  const add = await user.addUser(query)
  console.log(add)
  resp.send(add)
})

/*
用户列表
http://192.168.10.44:20091/user/list
*/
// app.get('/user/list', (req, res) => {
//   connection.query("SELECT * FROM `login`", function (err, results) {
//     if (err) {
//       res.send({
//         err: 1,
//         msg: "操作失败"
//       })
//     }
//     if (results.length > 0) {
//       res.send({
//         err: 0,
//         msg: "龚浩鑫操作成功",
//         data: results
//       })
//     }
//   })
// })

/*
添加用户
http://192.168.10.44:20091/user/list
*/
// app.post("/user/add", (req, res) => {
//   console.log(req)
//   console.log(req.params)
// })

// router.get('/getAllList', function (req, res) {
//   let project = { username: "haha", password: "667788909" }
//   let sqlStr = 'INSERT INTO project SET ?'
//   let connection = db.connection()
//   db.insert(connection, sqlStr, project, id => {
//     console.log("insert id:", id)
//   })
//   db.close(connection)
//   return;
// })


// 下载图片
// app.get("/file", (req, res) => {
//   res.download(imgPath)
// })

// // 显示图片
// app.get("/fileShow", (req, res) => {
//   res.sendFile(imgPath)
// })

// app.get("/downDoc", (req, res) => {
//   res.download(docPath)
// })



app.listen(20091, () => {
  console.log("服务开启")
})

