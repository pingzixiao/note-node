﻿
git clone 项目地址
例子：git clone https://gitee.com/z583829139/one01.git

缓存项目所有修改的文件
git add . 
提交本地所有修改的文件， -m 表示里面提交的内容做了什事
git commit -m "这里面是写注释"
直接提交到线上
git push

别人修改好了，把别人的内容拉取到你本的来
git pull

创建一个分支
git branch 分支名名
git branch hello
查看当前分支
git branch

查看所有分支
git branch -a

在当前自己创建的分支中提交当前分支到线上
git push --set-upstream origin 分支名
git push --set-upstream origin zhangxu

本地分支合并
git merge origin 分支名称

切换一个分支
git checkout 分支名
查看当前分支名称
git branch

查看当前修改了哪些文件
git status

修改本地分支
git branch -m oldName newName

git branch


回滚上一次提交：
查看提交日志找到哈希字符：
git log
回滚到上一次节点
git revert 哈唏值
git revert eb4b58fd5d0aa75f890084bfdc879b6cd2c5dd70


提交代码前，先拉取后提交

# 在master分支上
git pull // 拉取别人的代码
git add . // 缓存到本地暂存区
git commit -m //提交本地的代码
git push // 提交代码到线上

--------------------------------------------

# 创建一个新分支提交
git branch demo-pingzi新分支名名 // 创建一个新分支
git checkout demo-pingzi新分支名称 // 切换新支名称
{  code ....... 写完一个功能 }
# 在分支里面做的
git add . // 缓存到本地暂存区
git commit -m //提交本地的代码

git checkout master
git pull // 别人修改了拉取别人的代码
--
git merge origin/demo-pingzi新分支名称--
git merge origin demo-pingzi新分支名称----

远程拉取分支到本地并切换该分支
git checkout -b 远程分支 origin/远程分支
git checkout -b feature-01 origin/feature-0

git push

git push --set-upstream origin zhangxu

注意事项：
Please commit your changes or stash them before you switch branches.
Aborting
1、不提交不能切换分支


辙回操作
工作区：即自己当前分支所修改的代码，git add xx 之前的！
不包括 git add xx 和 git commit xxx 之后的。
git checkout -- a.txt // 丢弃某个文件，或者
git checkout -- . // 丢弃全部


#强制忽略跟踪
$ git update-index --assume-unchanged /master/text.txt(指定文件路径)   
   
#恢复强制忽略跟踪
$ git update-index --no-assume-unchanged /master/text.txt(指定文件路径)    

vim
vi 编辑器

:q 退出
:wq 保存退出
:!q



