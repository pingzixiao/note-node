
export const downloadDirect = (url => {
  console.log(url)
  const aTag = document.createElement("a")
  aTag.href = blobUrl
  aTag.click()
})

export const downloadByContent = ((content, titleName, type) => {
  console.log(content, titleName, type)
  const aTag = document.createElement("a")
  aTag.download = titleName
  const blob = new Blob([content], { type })
  const blobUrl = URL.createObjectURL(blob)
  aTag.href = blobUrl
  aTag.click()
  URL.revokeObjectURL(blob)
})

export const downloadByDataURL = ((content, filename, type) => {
  const aTag = document.createElement("a")
  aTag.download = filename
  const dataUrl = `data:${type};base64,${window.btoa(encodeURIComponent(content))}`
  aTag.href = dataUrl
  aTag.click()
})

export const downloadByBlob = ((blob, filename) => {
  const aTag = document.createElement("a")
  aTag.download = filename
  const blobUrl = URL.createObjectURL(blob)
  aTag.href = blobUrl
  aTag.click()
  URL.revokeObjectURL(blob)
})

export const base64ToBlob = ((base64, type) => {
  const byteCharacters = atob(base64)
  const byteNumbers = new Array(byteCharacters.length)
  for (let i = 0; i < byteCharacters.length; i++) {
    byteNumbers[i] = byteCharacters.charCodeAt(i)
  }
  const buffer = Uint8Array.from(byteNumbers)
  const blob = new Blob([buffer], { type })
  return blob;
})

