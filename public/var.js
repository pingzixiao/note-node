var a = null;
// console.log(a) // undefined
if (a === undefined) {
  console.log("a是undefined:", a)
} else {
  console.log("a不是undefined:", a)
}

var b = ""
console.log(typeof b) // string
console.log(b) // 空白

// console.log("a==b?:", a == b) // false
console.log("a==c?", a == c) // true(undefined == null)

var c = null;
console.log(c) // null



var a;
console.log("---", a === false)

var b = null
console.log(!b === true)

// 布尔运算：成功就是true(1) 失败是false(0)
var c = 0
console.log(!c === true)

// Not a Number
var d = NaN
console.log(!d === true)

var e = -0
console.log(!e === true)

var f = +0
console.log(!f === true)

var g = ""
console.log(!g === true)

//  1、能改变源数据的   

/*
2、不能修改源数据的
concat   join    slice  toString
indexOf lastIndexOf  includes
*/

// 数组合并
var a1 = ["ping", "gonghaoxi"]
var a2 = ["666"]
var a3 = a1.concat(a2)
console.log("合并后的数据：", a3)
console.log("源数组a1", a1)
console.log("源数组a2", a2)


// 字符串分隔，默认是逗号
var b1 = ["ping", "2", "3", "child"]
var b2 = b1.join("-");
console.log("数隔后的字符串：", b2)
console.log("源数组b1", b1)

// 按下标取数组
var c1 = ["slice", "slice2", "slice5"]
//  slice(下标，多少个)
var c2 = c1.slice(0, 3)
console.log("按逗号变成数组：", c2)

// 数组变成字符串
var d1 = ["piii", "haha", "4444"]
console.log(d1.toString())

// 查找字符串
var d1 = ["piii001", "haha002", "4444003", 1]
// 有值就是返回数组结果的下标，没有就返回-1
console.log(d1.indexOf(1))

// 从后往前找字符串
var e1 = ["4444", "4444", "666", "haha", "school"]
console.log(e1.lastIndexOf("4444", 1))

// 数组中是否存在指定的
var f1 = ["he", "school", "8"]
console.log(f1.includes(4))







