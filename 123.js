/*
箭头函数注意事项：
1、this问题，定义函数所在的对象，不是在运行所在对象
2、箭头函数里面没有arguments,用...
3、箭头函数不能当构造函数
*/

// 当函数参数只有一个，括号可以省略，但是没有参数时，括号不可以省略

// 无参
var fn1 = () => { }

// 两个参数
var fn2 = function (a) { }
var fn2 = a => { }

// 多个参数
var fn3 = function (a, b) { }
var fn3 = (a, b) => { }

// 可变参数
var fn4 = function (a, b, ...args) { }
var fn4 = (a, b, ...args) => { }

// https://blog.csdn.net/qq_32614411/article/details/80897256
// 返回只有一条语可以省略return
(a, b) => a + b

// args的用法